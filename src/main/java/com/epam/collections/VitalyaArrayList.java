package com.epam.collections;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class VitalyaArrayList<T> implements List<T> {
	private Object[] array;
	private static final int INITIAL_SIZE = 10;
	private static final int MAXIMUM_SIZE_BEFORE_INCREMENT = ((Integer.MAX_VALUE - 1) / 3) * 2;
	private static final int MAXIMUM_SIZE = Integer.MAX_VALUE - 8;
	private int innerArraySize = INITIAL_SIZE;
	private int size = 0;

	public VitalyaArrayList() {
		array = new Object[INITIAL_SIZE];
	}

	public VitalyaArrayList(int size) {
		array = new Object[size];
	}

	public VitalyaArrayList(Collection<? extends T> c) {
		if (c == null) {
			throw new IllegalArgumentException();
		}
		size = c.size();
		array = Arrays.copyOf(c.toArray(array), size);
	}

	public VitalyaArrayList(T[] array) {
		if (array == null) {
			throw new IllegalArgumentException();
		}
		size = array.length;
		array = Arrays.copyOf(array, array.length);
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public boolean isEmpty() {
		return size == 0;
	}

	@Override
	public boolean contains(Object o) {
		if (o == null) {
			new IllegalArgumentException();
		}
		for (Object obj : array) {
			if (o.equals(obj)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean add(T e) {
		if (e == null) {
			new IllegalArgumentException();
		}
		if (size == innerArraySize - 1) {
			increaseArray();
		}
		array[size] = e;
		size++;
		return true;
	}

	private void increaseArray() {
		int newArraySize;
		if (innerArraySize >= MAXIMUM_SIZE_BEFORE_INCREMENT) {
			newArraySize = MAXIMUM_SIZE;
		} else {
			newArraySize = innerArraySize * 3 / 2 + 1;
		}
		innerArraySize = newArraySize;
		Object[] increasedArray = new Object[newArraySize];
		System.arraycopy(array, 0, increasedArray, 0, size);
		array = increasedArray;
	}

	@Override
	public boolean remove(Object o) {
		if (o == null) {
			throw new IllegalArgumentException();
		}
		int removePosition = -1;
		for (int i = 0; i < size; i++) {
			if (o.equals(array[i])) {
				removePosition = i;
			}
		}
		if (removePosition == -1) {
			return false;
		}
		Object[] afterRemoving = new Object[innerArraySize];
		int lengthAfterRemoving = size - removePosition;
		System.arraycopy(array, 0, afterRemoving, 0, removePosition);
		System.arraycopy(array, removePosition + 1, afterRemoving, removePosition, lengthAfterRemoving);
		array = afterRemoving;
		size--;
		return true;
	}

	@Override
	public T remove(int index) {
		if ((index < 0) || (index > size)) {
			throw new IllegalArgumentException("Cannot remove element with index: " + index);
		}
		Object removed = array[index];
		Object[] afterRemoving = new Object[innerArraySize];
		int lengthAfterRemoving = size - index;
		System.arraycopy(array, 0, afterRemoving, 0, index);
		System.arraycopy(array, index + 1, afterRemoving, index, lengthAfterRemoving);
		array = afterRemoving;
		size--;
		return (T) removed;
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		if (c == null) {
			throw new IllegalArgumentException();
		}
		Iterator<?> it = c.iterator();
		int equalsCount = 0;
		while (it.hasNext()) {
			Object next = it.next();
			for (int i = 0; i < size; i++) {
				if (next.equals(array[i])) {
					equalsCount++;
					continue;
				}
			}
		}
		if (equalsCount < c.size()) {
			return false;
		}
		return true;
	}

	@Override
	public boolean addAll(Collection<? extends T> c) {
		if (c == null) {
			throw new IllegalArgumentException();
		}
		if (c.size() > MAXIMUM_SIZE - size) {
			throw new IllegalArgumentException("Too big collection with size: " + c.size());
		}
		Iterator<?> it = c.iterator();
		while (it.hasNext()) {
			T next = (T) it.next();
			add(next);
		}
		return true;
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		if (c == null) {
			throw new IllegalArgumentException();
		}
		Iterator<?> it = c.iterator();
		while (it.hasNext()) {
			Object next = it.next();
			for (int i = 0; i < size; i++) {
				if (next.equals(array[i])) {
					remove(i);
				}
			}
		}
		return true;
	}

	@Override
	public void clear() {
		size = 0;
		array = new Object[innerArraySize];
	}

	@Override
	public T get(int index) {
		if ((index < 0) || (index >= size)) {
			throw new IllegalArgumentException("Incorrect index: " + index);
		}
		return (T) array[index];
	}

	@Override
	public T set(int index, T element) {
		if ((index < 0) || (index >= size)) {
			throw new IllegalArgumentException("Incorrect index: " + index);
		}
		if (element == null) {
			throw new IllegalArgumentException();

		}
		array[index] = element;
		return (T) array[index];
	}

	@Override
	public int indexOf(Object o) {
		if (o == null) {
			throw new IllegalArgumentException();

		}
		for (int i = 0; i < size; i++) {
			if (o.equals(array[i])) {
				return i;
			}
		}
		return -1;
	}

	@Override
	public int lastIndexOf(Object o) {
		if (o == null) {
			throw new IllegalArgumentException();

		}
		for (int i = size - 1; i >= 0; i--) {
			if (o.equals(array[i])) {
				return i;
			}
		}
		return -1;
	}

	@Override
	public List<T> subList(int fromIndex, int toIndex) {
		List<T> resultList = (List<T>) new VitalyaArrayList<>(Arrays.copyOfRange(array, fromIndex, toIndex));
		return resultList;
	}

	@Override
	public String toString() {
		return Arrays.toString(Arrays.copyOfRange(array, 0, size));
	}

	@Override
	public Object[] toArray() {
		return Arrays.copyOf(array, size);
	}

	@Override
	public <T> T[] toArray(T[] a) {
		return (T[]) Arrays.copyOf(array, size);
	}

	@Override
	public Iterator<T> iterator() {
		class Iter implements Iterator<T> {
			private int counter = 0;

			@Override
			public boolean hasNext() {
				if (counter < size) {
					return true;
				}
				return false;
			}

			@Override
			public T next() {
				return get(counter++);
			}
		}
		return new Iter();
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public ListIterator<T> listIterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ListIterator<T> listIterator(int index) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void add(int index, T element) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean addAll(int index, Collection<? extends T> c) {
		// TODO Auto-generated method stub
		return false;
	}

}
