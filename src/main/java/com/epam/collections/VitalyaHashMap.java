package com.epam.collections;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

public class VitalyaHashMap<K, V> implements Map<K, V> {
	private static final int DEFAULT_SIZE = 16;
	private static final double LOAD_FACTOR = 0.75;
	private Entry<K, V>[] entries;
	private int capacity = DEFAULT_SIZE;
	private int size;
	private int filledBuckets;

	public VitalyaHashMap() {
		entries = new Entry[DEFAULT_SIZE];
	}

	public VitalyaHashMap(int capacity) {
		entries = new Entry[capacity];
		this.capacity = capacity;
	}

	private int hash(int hashCode) {
		return hashCode % capacity;
	}

	static class Entry<K, V> implements Map.Entry<K, V> {
		private K key;
		private V value;
		private Entry<K, V> next;

		public Entry() {
		}

		public Entry(K key, V value) {
			this.key = key;
			this.value = value;
		}

		@Override
		public K getKey() {
			return key;
		}

		@Override
		public V getValue() {
			return value;
		}

		@Override
		public V setValue(V value) {
			this.value = value;
			return value;
		}

		public Entry<K, V> getNext() {
			return next;
		}

		public void setNext(Entry<K, V> next) {
			this.next = next;
		}

		public boolean hasNext() {
			return next != null;
		}
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public boolean isEmpty() {
		return size == 0;
	}

	@Override
	public boolean containsKey(Object key) {
		return get(key) != null;
	}

	@Override
	public boolean containsValue(Object value) {
		if (value == null) {
			throw new IllegalArgumentException("Illegal value = " + value);
		}
		for (Entry<K, V> entry : entries) {
			if (value.equals(entry.value)) {
				return true;
			}
		}
		Entry<K, V> currentEntry;
		for (Entry<K, V> entry : entries) {
			currentEntry = entry;
			while (currentEntry.hasNext()) {
				currentEntry = currentEntry.next;
				if (value.equals(currentEntry.value)) {
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public V get(Object key) {
		if (key == null) {
			throw new IllegalArgumentException("Illegal key = " + key);
		}
		int keyHash = key.hashCode();
		int position = hash(keyHash);
		Entry<K, V> current = entries[position];
		do {
			if (key.equals(current.key)) {
				return current.value;
			}
			current = current.next;
		} while (current != null);
		return null;
	}

	// ADD RESIZING HERE!!!!!!!!!!!!--_>
	@Override
	public V put(K key, V value) {
		int hashCode = key.hashCode();
		int bucketPosition = hash(hashCode);
		Entry<K, V> currentEntry = entries[bucketPosition];
		do {
			if (key.equals(currentEntry.key)) {
				currentEntry.value = value;
				return value;
			}
			currentEntry = currentEntry.next;
		} while (currentEntry != null);
		entries[bucketPosition] = new Entry(key, value);
		return value;
	}

	@Override
	public V remove(Object key) {
		if (key == null) {
			throw new IllegalArgumentException("Illegal key = " + key);
		}
		int keyHash = key.hashCode();
		int position = hash(keyHash);
		Entry<K, V> current = entries[position];
		Entry<K, V> previous = null;
		V value = null;
		do {
			if (key.equals(current.key)) {
				value = current.value;
				if (previous == null) {
					current = null;
				} else {
					previous.next = current.next;
				}
				return value;
			}
			previous = current;
			current = current.next;
		} while (current != null);
		return value;
	}

	@Override
	public void clear() {
		for (Entry<K, V> entry : entries) {
			entry = null;
		}
	}

	@Override
	public void putAll(Map<? extends K, ? extends V> m) {
		// TODO Auto-generated method stub

	}

	@Override
	public Set<K> keySet() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection<V> values() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Set<java.util.Map.Entry<K, V>> entrySet() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		for (Entry<K, V> entry : entries) {
			Entry<K, V> currentEntry = entry;
			do {
				sb.append(currentEntry.value.toString());
				sb.append(", ");
				currentEntry = currentEntry.next;
			} while (currentEntry != null);
		}
		sb.delete(sb.length() - 1, sb.length());
		sb.append("]");
		return sb.toString();
	}
}
