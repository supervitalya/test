package com.epam.collections;

import java.util.Collection;
import java.util.Deque;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;

public class VitalyaLinkedList<T> implements List<T>, Deque<T> {
	private int size;
	private Node<T> first;
	private Node<T> last;

	static class Node<T> {
		private T element;
		private Node<T> next;
		private Node<T> previous;

		Node(T t) {
			this.element = t;
		}
	}

	public VitalyaLinkedList() {
	}

	public VitalyaLinkedList(Collection<? extends T> c) {
		addAll(c);
	}

	@Override
	public void addFirst(T e) {
		Node<T> node = new Node<>(e);
		if (size == 0) {
			first = node;
			last = node;
			size++;
			return;
		}
		if (size == 1) {
			first.previous = node;
			node.next = first;
			first = node;
			size++;
			return;
		}
		node.next = first;
		first.previous = node;
		first = node;
		size++;
	}

	@Override
	public void addLast(T e) {
		if (size == 0) {
			addFirst(e);
			return;
		}
		Node<T> node = new Node<>(e);
		if (size == 1) {
			first.next = node;
			node.previous = first;
			last = node;
			size++;
			return;
		}
		node.previous = last;
		last.next = node;
		last = node;
		size++;
	}

	@Override
	public T removeFirst() {
		if (size == 0) {
			throw new NoSuchElementException();
		}
		Node<T> firstNode = first;
		first = first.next;
		first.previous = null;
		size--;
		return firstNode.element;
	}

	@Override
	public T removeLast() {
		if (size == 0) {
			throw new NoSuchElementException();
		}
		Node<T> lastNode = last;
		last = last.previous;
		last.next = null;
		size--;
		return lastNode.element;
	}

	@Override
	public T pollFirst() {
		if (size == 0) {
			return null;
		}
		Node<T> firstNode = first;
		first = first.next;
		first.previous = null;
		size--;
		return firstNode.element;
	}

	@Override
	public T pollLast() {
		if (size == 0) {
			return null;
		}
		Node<T> lastNode = last;
		last = last.previous;
		last.next = null;
		size--;
		return lastNode.element;
	}

	@Override
	public T getFirst() {
		if (size == 0)
			throw new NoSuchElementException();
		return first.element;
	}

	@Override
	public T getLast() {
		if (size == 0)
			throw new NoSuchElementException();
		return last.element;
	}

	@Override
	public T peekFirst() {
		if (size == 0) {
			return null;
		}
		return first.element;
	}

	@Override
	public T peekLast() {
		if (size == 0) {
			return null;
		}
		return last.element;
	}

	@Override
	public boolean add(T e) {
		addFirst(e);
		return true;
	}

	@Override
	public boolean remove(Object o) {
		if (o == null) {
			throw new IllegalArgumentException();
		}
		if (size == 0) {
			return false;
		}
		Node<T> currentNode = first;
		do {
			if (currentNode.element.equals(o)) {
				Node<T> previousNode = currentNode.previous;
				Node<T> nextNode = currentNode.next;
				previousNode.next = nextNode;
				nextNode.previous = previousNode;
				return true;
			}
		} while (currentNode != null);
		return false;
	}

	@Override
	public T remove() {
		if (size == 0) {
			throw new NoSuchElementException();
		}
		Node<T> firstNode = first;
		first = first.next;
		first.previous = null;
		return firstNode.element;

	}

	@Override
	public T poll() {
		if (size == 0) {
			return null;
		}
		Node<T> firstNode = first;
		first = first.next;
		first.previous = null;
		return firstNode.element;
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public boolean isEmpty() {
		return size == 0;
	}

	@Override
	public boolean contains(Object o) {
		if (o == null) {
			throw new IllegalArgumentException();
		}
		if (size == 0) {
			return false;
		}
		Node<T> currentNode = first;
		do {
			if (currentNode.element.equals(o)) {
				return true;
			}
		} while (currentNode != null);
		return false;
	}

	@Override
	public Object[] toArray() {
		if (size == 0)
			return null;
		Object[] array = new Object[size];
		Node<T> current = first;
		int counter = 0;
		do {
			array[counter++] = current.element;
			current = current.next;
		} while (current != null);
		return array;
	}

	@Override
	public void clear() {
		first = null;
		last = null;
		size = 0;
	}

	@Override
	public T get(int index) {
		if ((index < 0) || (index >= size)) {
			throw new IllegalArgumentException("Illegal index: " + index);
		}
		Node<T> currentNode = first;
		for (int i = 0; i < index; i++) {
			currentNode = currentNode.next;
		}
		return currentNode.element;
	}

	@Override
	public T element() {
		if (size == 0) {
			throw new NoSuchElementException();
		}
		return first.element;
	}

	@Override
	public T peek() {
		if (size == 0) {
			return null;
		}
		return first.element;
	}

	@Override
	public String toString() {
		if (size == 0) {
			return "[]";
		}
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		Node<T> currentNode = first;
		do {
			sb.append(currentNode.element.toString());
			sb.append(", ");
			currentNode = currentNode.next;
		} while (currentNode != null);
		sb.delete(sb.length() - 2, sb.length());
		sb.append("]");
		return sb.toString();
	}

	@Override
	public boolean offerFirst(T e) {
		addFirst(e);
		return true;
	}

	@Override
	public boolean offerLast(T e) {
		addLast(e);
		return true;
	}

	@Override
	public boolean addAll(Collection<? extends T> c) {
		Iterator<? extends T> it = c.iterator();
		while (it.hasNext()) {
			addFirst(it.next());
		}
		return true;
	}

	@Override
	public boolean offer(T e) {
		add(e);
		return true;
	}

	@Override
	public void push(T e) {
		addFirst(e);
	}

	@Override
	public T pop() {
		return removeFirst();
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		Iterator<?> it = c.iterator();
		while (it.hasNext()) {
			remove(it.next());
		}
		return true;
	}

	@Override
	public int indexOf(Object o) {
		if (o == null) {
			throw new IllegalArgumentException();
		}
		if (size == 0) {
			return -1;
		}
		int index = -1;
		Node<T> currentNode = first;
		do {
			index++;
			if (o.equals(currentNode.element)) {
				return index;
			}
			currentNode = currentNode.next;
		} while (currentNode != null);
		return -1;
	}

	@Override
	public int lastIndexOf(Object o) {
		if (o == null) {
			throw new IllegalArgumentException();
		}
		if (size == 0) {
			return -1;
		}
		int index = -1;
		Node<T> currentNode = last;
		do {
			index++;
			if (o.equals(currentNode.element)) {
				return size - index - 1;
			}
			currentNode = currentNode.previous;
		} while (currentNode != null);
		return -1;
	}

	@Override
	public boolean removeFirstOccurrence(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean removeLastOccurrence(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Iterator<T> descendingIterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> T[] toArray(T[] a) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean addAll(int index, Collection<? extends T> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public T set(int index, T element) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void add(int index, T element) {
		// TODO Auto-generated method stub

	}

	@Override
	public T remove(int index) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ListIterator<T> listIterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ListIterator<T> listIterator(int index) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<T> subList(int fromIndex, int toIndex) {
		// TODO Auto-generated method stub
		return null;
	}

}
