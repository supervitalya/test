package com.epam.test.atomic;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class AtomicTest {
	public static AtomicInteger atom = new AtomicInteger();
	public static volatile int volat = 0;
	public static int usual = 0;
	public static final int THREADS_AMOUNT = 10000;

	public static void main(String[] args) throws Throwable {
		for (int i = 0; i < THREADS_AMOUNT; i++) {
			new AtomicIncrementor(atom).start();
			;
		}
		TimeUnit.SECONDS.sleep(3);
		System.out.println("Atomic: " + atom);
		Lock lock = new ReentrantLock();
		for (int i = 0; i < THREADS_AMOUNT; i++) {
			new LockingIncrementor(lock).start();
		}
		TimeUnit.SECONDS.sleep(3);
		System.out.println("Usual: " + usual);
		for (int i = 0; i < THREADS_AMOUNT; i++) {
			new VolatileIncrementor().start();
		}
		TimeUnit.SECONDS.sleep(3);
		System.out.println("Volatile: " + volat);
	}
}

class AtomicIncrementor extends Thread {
	AtomicInteger ai;

	AtomicIncrementor(AtomicInteger ai) {
		this.ai = ai;
	}

	@Override
	public void run() {
		for (int i = 0; i < 100; i++) {
			ai.incrementAndGet();
		}
	}
}

class LockingIncrementor extends Thread {
	private Lock lock;

	LockingIncrementor(Lock lock) {
		this.lock = lock;
	}

	public void run() {
		for (int i = 0; i < 100; i++) {
			try {
				lock.lock();
				AtomicTest.usual++;
			} finally {
				lock.unlock();
			}

		}
	}
}

class VolatileIncrementor extends Thread {
	public void run() {
		for (int i = 0; i < 100; i++) {
			AtomicTest.volat++;
		}
	}
}
