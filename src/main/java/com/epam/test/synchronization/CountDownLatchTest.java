package com.epam.test.synchronization;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class CountDownLatchTest {
	public static final int THREADS_COUNT = 10;
	public static final int THREADS_AT_MOMENT_COUNT = 5;

	public static void main(String[] args) throws Throwable {
		CountDownLatch latch = new CountDownLatch(THREADS_AT_MOMENT_COUNT);
		Thread[] threads = new Thread[THREADS_COUNT];
		for (int i = 0; i < THREADS_COUNT; i++) {
			threads[i] = new CountDownLatchSpeaker(latch);
		}
		for (Thread t : threads) {
			t.start();
		}
	}

}

class CountDownLatchSpeaker extends Thread {
	private static final Lock lock = new ReentrantLock();
	private String[] replies = { "One", "Two", "Three", "Four" };
	private CountDownLatch latch;

	public CountDownLatchSpeaker(CountDownLatch latch) {
		this.latch = latch;
	}

	@Override
	public void run() {
		try {
			try {
				lock.lock();
				System.out.println("Latch count: " + latch.getCount());
				TimeUnit.MILLISECONDS.sleep(1000);
				latch.countDown();
			} finally {
				lock.unlock();
			}
			latch.await();
			for (String reply : replies) {
				System.out.println(this.getName() + " : " + reply);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
