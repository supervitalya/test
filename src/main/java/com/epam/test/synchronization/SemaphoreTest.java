package com.epam.test.synchronization;

import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class SemaphoreTest {
	public static final int THREADS_COUNT = 20;
	public static final int THREADS_AT_MOMENT_COUNT = 3;

	public static void main(String[] args) throws Throwable {
		Semaphore s = new Semaphore(THREADS_AT_MOMENT_COUNT);
		Thread[] threads = new Thread[THREADS_COUNT];
		for (int i = 0; i < THREADS_COUNT; i++) {
			threads[i] = new Speaker(s);
		}
		for (Thread t : threads) {
			t.start();
		}
	}

}

class Speaker extends Thread {
	private String[] replies = { "One", "Two", "Three", "Four" };
	private Semaphore sema;

	public Speaker(Semaphore semaphore) {
		sema = semaphore;
	}

	@Override
	public void run() {
		try {
			sema.acquire();
			for (String reply : replies) {
				System.out.println(this.getName() + " : " + reply);
				TimeUnit.MILLISECONDS.sleep(300);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			sema.release();
			/*
			 * sema.release(); You can make more releases. It will increase
			 * amount of permits. Or you can vary it depending on workload.
			 */
		}
	}
}
