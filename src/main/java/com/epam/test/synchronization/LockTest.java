package com.epam.test.synchronization;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class LockTest {
	public static final int THREADS_COUNT = 10;
	public static final int THREADS_AT_MOMENT_COUNT = 2;

	public static void main(String[] args) throws Throwable {
		Lock l = new ReentrantLock();
		Thread[] threads = new Thread[THREADS_COUNT];
		for (int i = 0; i < THREADS_COUNT; i++) {
			threads[i] = new LockingSpeaker(l);
		}
		for (Thread t : threads) {
			t.start();
		}
	}

}

class LockingSpeaker extends Thread {
	private String[] replies = { "One", "Two", "Three", "Four" };
	private Lock lock;
	private static int counter = 0;

	public LockingSpeaker(Lock lock) {
		this.lock = lock;
	}

	@Override
	public void run() {
		lock.lock();
		counter++;
		System.out.println("counter: " + counter);
		try {
			for (String reply : replies) {
				System.out.println(this.getName() + " : " + reply);
				TimeUnit.MILLISECONDS.sleep(300);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			lock.unlock();
		}
	}
}
