package com.epam.test.synchronization;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;

public class CyclicBarrierTest {
	public static final int THREADS_COUNT = 15;
	public static final int THREADS_AT_MOMENT_COUNT = 5;

	public static void main(String[] args) throws Throwable {
		CyclicBarrier barrier = new CyclicBarrier(THREADS_AT_MOMENT_COUNT);
		Thread[] threads = new Thread[THREADS_COUNT];
		for (int i = 0; i < THREADS_COUNT; i++) {
			threads[i] = new CyclicSpeaker(barrier);
		}
		for (Thread t : threads) {
			System.out.println("Thread started");
			t.start();
			TimeUnit.MILLISECONDS.sleep(1000);
		}
	}
}

class CyclicSpeaker extends Thread {
	private String[] replies = { "One", "Two", "Three", "Four" };
	private CyclicBarrier barrier;

	public CyclicSpeaker(CyclicBarrier barrier) {
		this.barrier = barrier;
	}

	@Override
	public void run() {
		try {
			for (String reply : replies) {
				barrier.await();
				System.out.println(this.getName() + " : " + reply);
			}
		} catch (InterruptedException | BrokenBarrierException e) {
			e.printStackTrace();
		}
	}
}
