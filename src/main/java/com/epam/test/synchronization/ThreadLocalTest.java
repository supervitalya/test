package com.epam.test.synchronization;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class ThreadLocalTest {
	public static final int TASKS_NUMBER = 5;

	public static void main(String[] args) throws Throwable {
		SomeInformation si = new SomeInformation();
		for (int i = 0; i < TASKS_NUMBER; i++)
			new ThreadWithLocal(si).start();
	}
}

class ThreadWithLocal extends Thread {
	private SomeInformation si;

	ThreadWithLocal(SomeInformation s) {
		si = s;
	}

	@Override
	public void run() {
		si.setLocalValue(new Random().nextInt(200));
		for (int i = 0; i < 10; i++) {
			try {
				TimeUnit.MILLISECONDS.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.out.println(si.getLocalValue());
		}
	}
}

class SomeInformation {
	private ThreadLocal<Integer> threadLocal;
	private Random random = new Random();

	SomeInformation() {
		threadLocal = new ThreadLocal<>();
		threadLocal.set(random.nextInt(100));
	}

	public void setLocalValue(int value) {
		threadLocal.set(value);
	}

	public int getLocalValue() {
		return threadLocal.get();
	}
}
