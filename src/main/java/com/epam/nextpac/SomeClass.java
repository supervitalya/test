package com.epam.nextpac;

public abstract class SomeClass {

	public abstract int hashCode();

	public static void main(String[] args) {
		System.out.println(new SecondClass().hashCode());
	}
}

class SecondClass extends SomeClass {
	static final int hc;

	static {
		hc = 228;
	}

	@Override
	public int hashCode() {

		return hc;
	}

}
